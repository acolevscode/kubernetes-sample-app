# README #

This is a small sample project to tour the user through setting up a kubernetes project in their local development area.

## Environment Setup ##

Follow the instructions in the Kubernetes-Documentation.pdf file.

## Requirements ##

* NodeJS
* iTerm
* Docker
* Docker Registry
* Kubernetes with 2 nodes

## Setup ##

### Open the correct terminal ###

* Click the kube-cluster icon
* Click Preset OS shell

### Build the node application ###

Navigate to the 'app' folder and run the npm installation

```
$ cd app
$ npm install
$ node server.js
```

You should see 'Hello world' at http://localhost:8080/

Hit Ctrl + c to end the node application.

Return to the base project folder for the following sections.

## Docker ##

### Build the docker image ###

docker build -t <your ip address>:5000/<your username>/kubernetes-sample-app:v1 .

```
$ docker build -t 192.168.1.100:5000/auxxx/kubernetes-sample-app:v1 .
```

### Run the docker image ###

docker run -d -p 8080:8080 <your ip address>:5000/<your username>/kubernetes-sample-app:v1

```
$ docker run -d -p 8080:8080 192.168.1.100:5000/auxxx/kubernetes-sample-app:v1
```

Once the container is running you should be able to open a browser at: http://localhost:8080/ and see "Hello world".

### View the container information ###

```
$ docker ps
```

### Stop the Container ###

Use the Container ID from the docker ps information for the kubernetes-sample-app.

```
$ docker stop 924b99b39f04
```

### Remove the stopped Container ###

```
$ docker rm 924b99b39f04
```

### Push the image into the local repository ###

docker push <your ip address>:5000/<your username>/kubernetes-sample-app:v1

```
$ docker push 192.168.1.100:5000/auxxx/kubernetes-sample-app:v1
```

## Kubernetes ##

Edit the app.yaml file to point to the docker image that you pushed into your private registry.

* Edit app.yaml
* change `image: 192.168.1.100:5000/auxxx/kubernetes-sample-app:v1` to your image

Run the kubernetes application.

```
$ kubectl create -f app.yaml
```

Stop the application.

```
$ kubectl delete -f app.yaml
```
